#!/usr/local/bin/bash

# setup cron */5 * * * * for every 5 mins

# silent mode
curl -s https://api.coinmarketcap.com/v1/ticker/\?convert\=AUD -o top100_AUD_5mins.json
if [ $? -eq 0 ]; then
   echo '===> Data is downloaded.';
else
   echo '===< Not data download.';
fi

pgfutter jsonobj top100_AUD_5mins.json
if [ $? -eq 0 ]; then
    echo Push JSON to Postgresql.
else
    echo Push to DB fail.
fi

# -X no profile
# \t for no heading return aka Tuple Only mode.
# -q is quite mode. So when \t occur, not output
n_inserted=$(psql -Xq -d postgres <<EOF
\t
DROP TABLE IF EXISTS tentative;

CREATE TEMP TABLE IF NOT EXISTS tentative AS
SELECT (json_populate_recordset(null::import.token_type, data)).*
FROM import.top100_AUD_5mins;

INSERT INTO import.token
SELECT * FROM tentative
WHERE id IN (
	SELECT
		src.id    --new id
	FROM import.token tar
	JOIN tentative    src ON tar.id = src.id
	GROUP BY
		src.id,
		src.last_updated
	HAVING src.last_updated > max(tar.last_updated)
);

SELECT max(last_updated) as last_updated FROM import.token;
EOF
);

echo Last ON: `date -d @"$n_inserted"`;

# dont use $?
#if [ $n_inserted -gt 0 ]; then
#	echo '===> New record Inserted.';
#else
#	echo '===< Nothing Inserted.';
#fi



