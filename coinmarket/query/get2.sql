create table test as
    select i, now() - '6 months'::interval * random() as ts
    from generate_series(1,100000) i;

select count(*), min(ts), max(ts) from test;

--group by day
select 
	date_trunc( 'day', ts ), 
	count(*)
from test
group by 1
order by 1 desc 
limit 10;
--group by hour
select date_trunc( 'hour', ts ), count(*)
from test
group by 1
order by 1 desc limit 5;
--5 mins chunk
select now(), 'epoch'::timestamptz + '300 seconds'::interval * (extract(epoch from now())::int4 / 300);
--1970-01-01 10:00:00+10
select 'epoch'::timestamptz;

CREATE FUNCTION ts_round( timestamptz, INT4 ) RETURNS TIMESTAMPTZ AS $$
    SELECT 'epoch'::timestamptz + '1 second'::INTERVAL * ( $2 * ( extract( epoch FROM $1 )::INT4 / $2 ) );
$$ LANGUAGE SQL;

select now(), ts_round( now(), 629 );

--group by day
select date_trunc('minute', ts_round( ts, 15*60 )),
	   count(*)
from test
group by 1
order by 1 desc 
limit 10;

-- select extract( TIMESTAMPTZ FROM to_char(now()))