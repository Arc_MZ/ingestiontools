--recordset
SELECT json_populate_recordset(null::token_type, data) 
FROM import.coinmarketcap_aud_tick;

--recordset to table columns
INSERT INTO token
SELECT (json_populate_recordset(null::token_type, data)).* 
FROM import.coinmarketcap_aud_tick;

select * from token;

drop type token_type;
CREATE type token_type AS (
	"id"                  text,
	"name"                text,
	"symbol"              text,
	"rank"                int,
	"price_usd"           numeric,
	"price_btc"           numeric,
	"price_aud"           numeric,
	"24h_volume_usd"      numeric,
	"market_cap_usd"      numeric,
	"available_supply"    numeric,
	"total_supply"        numeric,
	"max_supply"          numeric,
	"percent_change_1h"   numeric,
	"percent_change_24h"  numeric,
	"percent_change_7d"   numeric,
	"last_updated"        text,         --come back
	"24h_volume_aud"      numeric,
	"market_cap_aud"      numeric
);

drop table token;
create table token (
	"id"                  text,
	"name"                text,
	"symbol"              text,
	"rank"                int,
	"price_usd"           numeric,
	"price_btc"           numeric,
	"price_aud"           numeric,
	"24h_volume_usd"      numeric,
	"market_cap_usd"      numeric,
	"available_supply"    numeric,
	"total_supply"        numeric,
	"max_supply"          numeric,
	"percent_change_1h"   numeric,
	"percent_change_24h"  numeric,      --numeric(3,2)
	"percent_change_7d"   numeric,
	"last_updated"        text,         --timestamp 
	"24h_volume_aud"      numeric,
	"market_cap_aud"      numeric
);
