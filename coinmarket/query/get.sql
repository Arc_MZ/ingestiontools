select * 
from
(
	select
		dense_rank() over ( partition by rank 
							order by last_updated::int ) as mr,
-- 		date_trunc('minute', ts_round( to_timestamp(last_updated::int), 15*60 )) as "partition",
		rank,
		name,
		symbol,
		price_usd,
		price_btc,
	-- 	round(coalesce("24h_volume_usd", 0) / 1000000, 2)	 as "24h_volume_usd in m",
	-- 	round(coalesce(market_cap_usd, 0)   / 1000000000, 2)    as "market_cap_usd in b",
		available_supply,
	-- 	round(total_supply / 1000000000, 2) as "total_supply in b",
	-- 	round(max_supply / 1000000000, 2)   as "max supply in b",
		percent_change_1h,
		percent_change_24h,
		percent_change_7d,
		to_timestamp(last_updated::int) as last_updated
-- 		TO_CHAR(to_timestamp(last_updated::int), 'dd-Mon-YYYY')
	from token
	where to_timestamp(last_updated::int) >= (localtimestamp - interval '125 minute')
) src
-- where src.last_updated >= (localtimestamp - interval '25 minute')
order by src.rank 
limit 300;

-- select to_timestamp(last_updated::int), 
-- 	  * 
-- from token where name like 'Bitcoin' order by last_updated desc;
-- 
-- select coalesce(null, 1) / 1000000;
-- select (current_timestamp - interval '1 day'); --2018-01-29 12:50:37.95394+11
-- 
-- show datestyle; --ISO, DMY
-- select localtimestamp; --2018-01-30 13:06:57.665584
-- select now();		   --2018-01-30 13:07:06.048831+11
-- select last_updated::timestamp from token limit 1;
-- 
-- select 1::numeric/2;
-- select * from token where last_updated = '1516749862';
-- 
-- select '1516749862'::timestamp;
-- select to_timestamp(1516749862);
-- 
-- select column_name || ',' from information_schema.columns where table_name='token';
-- select column_name, data_type, * from information_schema.columns where table_name='token';