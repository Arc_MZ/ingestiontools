

### Add These to CronTab
PATH=$PATH:/Users/michael.zhuang/.cargo/bin:/usr/local/Cellar/awless/0.1.1/bin:/usr/local/opt/coreutils/libexec/gnubin:/usr/local/opt/sphinx-doc/bin:/Applications/calibre.app/Contents/console.app/Contents/MacOS/:/usr/local/opt/libxslt/bin:/usr/local/opt/libxml2/bin:/usr/local/opt/openssl/bin:/usr/local/sbin:/Library/Frameworks/Mono.framework/Versions/Current/Commands:/Library/Java/JavaVirtualMachines/jdk1.8.0_112.jdk/Contents/Home/bin:/Users/michael.zhuang/anaconda/bin/:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Users/michael.zhuang/.local/bin:/Users/michael.zhuang/scala-2.12.2/bin:/usr/local/Cellar/go/1.9.2/bin:/usr/local/opt/go/libexec/bin:/usr/local/Cellar/macvim/8.0-142/bin:/usr/local/Cellar/apache-spark/2.2.0/libexec/sbin:/usr/local/mysql/bin:/usr/local/Cellar/gcc/7.2.0/bin:/usr/local/opt/fzf/bin
#*/5 * * * * env > ~/cron_env_output.txt
*/5 * * * * ~/TOOLS/ingestiontools/coinmarket/iter.sh

